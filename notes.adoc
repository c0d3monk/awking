= AWK'ing
:toc: left

Learn AWK using examples

== Preface
* This a minified guide to get hands on with `awk`

* For an extensive guide please visit https://www.gnu.org/software/gawk/manual/html_node/Getting-Started.html#Getting-Started[GNU AWK]

== History

* `awk` comes from Alfred V. **A**ho, Peter J. **W**einberger &
Brain W. **K**ernighan
* Original version came out in 1977 at AT & T Bell Labs


== Getting Started
* Basic function of `awk` is to search files for lines (or other units of text)
that contain certain patterns.
* an `awk` programs consists of a series of rules
* Syntactically, a rule consists of a _pattern_ followed by an _action_
* _action_ is enclosed in braces to separate from _pattern_
* Newlines usually separate rules
* It looks like
```
pattern {action}
pattern {action}
```
=== How to run `awk` programs

* _program_ directly passed in CLI:
`awk 'program' input1-file1 input-file2 ...`

* From file:
`awk -f program-file input-file1 input-file2 ...`

* Without input files, rather take input from STDIN:
`awk 'program'`
* It continues to read from STDIN until you indicate EOF by typing `Ctrl-d`

=== The *BEGIN* and the *END* keywords
* `awk` executes statements associated with *BEGIN* before reading any input.
* Example: `awk 'BEGIN { print "Works"}'`

* `awk` executes statements associated with *END* after reading all of the input
* Example:

`awk 'END {print NR}' input-file # prints the number of lines`


=== Using `awk` as `cat` example

```
$ awk '{print}'
Hi there
Hi there
testing
testing
Ctrl-d
```

=== Executable `awk` programs
```
#!/usr/bin/awk -f

# This is a comment

BEGIN { print "Works isn\47t" }
```

```
$ chmod +x filename
$ filename
Works
```

=== Simple example
```
# input-file1
Amelia       555-5553     amelia.zodiacusque@gmail.com    F
Anthony      555-3412     anthony.asserturo@hotmail.com   A
Becky        555-7685     becky.algebrarum@gmail.com      A
Bill         555-1675     bill.drowning@hotmail.com       A
Broderick    555-0542     broderick.aliquotiens@yahoo.com R
Camilla      555-2912     camilla.infusarum@skynet.be     R
Fabius       555-1234     fabius.undevicesimus@ucb.edu    F
Julie        555-6699     julie.perscrutabor@skeeve.com   F
Martin       555-6480     martin.codicibus@hotmail.com    A
Samuel       555-3430     samuel.lanceolis@shu.edu        A
Jean-Paul    555-2127     jeanpaul.campanorum@nyu.edu     R

```

```
$ awk '/li/ {print $0} ' input-file1
```

* The text surround by `/` is the pattern/regex to match
* `print $0` means print the current line
* Only `print` statement would have also done the same

[NOTE]
====
* In an `awk` rule, either the pattern or the action can be omitted, but
not both.
* If the _pattern_ is omitted, then the _action_ is performed for every input
line
* If the _action_ is omitted, the default action is to print all lines that
match the pattern
====

=== Practical examples
* *Find all the users who don't have shell login permission in unix*

`awk -F: '/nologin/ {print $1} ' /etc/passwd`

* *Print every line that is longer that 80 chars long*

`awk 'length($0) > 80 {print $0}' /etc/passwd`

* *Print the longest line from a file with char length*

`awk '{ if (length($0) > max) { max = length($0); line = $0}} END {print max; print line}' input-file`

NOTE: The code associated with `END` executes after all input has been read,
  opposite of `BEGIN`

* *Delete blank lines from a file OR print every line that has atleast 1 field:*

`awk 'NF > 0' input-file1`

* *Print total Kb used by files in a directory:*

`ls -l /etc/  | awk '{x += $5} END {print "Total Kb: ", x/1024}'`

* *Sorted list of login names of all users:*

`awk -F: '{print $1}' /etc/passwd  | sort`

* *Count the lines in a file:*

`awk 'END {print NR}' /etc/passwd`

* *Print even numbered lines in a file*

`awk 'NR % 2 == 0' input-file`

* *Example with 2 rules*

`awk '/nologin/ {print $0}; /false/ {print $0}' /etc/passwd`

[NOTE]
====
* The `awk` reads the input files one line at a time
* For each line, `awk` tries the patterns of each rule
* If several patterns match, then sevral actions execute in the order in which
they appear in the `awk` program
* If no patterns match, then no actions run
====

== Running `awk`
* +++<u>Two ways:</u>+++.
** Explicit program
`awk [options] [--] 'program' file ...`
** One or more program files
`awk [options] -f p1 -f p2 [--] file ...`

=== Important Command Line options

```
-f program-file
--file program-file
       Read the AWK program source from the file program-file, instead of from
       the first command line argument.  Multiple -f (or --file) options may be
       used.

-F fs
--field-separator fs
       Use fs for the input field separator (the value of the FS predefined
       variable).

-v var=val
--assign var=val
       Assign the value val to the variable var, before execution of the
       program begins.  Such variable values are available to the BEGIN rule of
       an AWK program.
```

* For more detailed options, `man awk`

=== The *AWKPATH* environment variable
* Of the `-f` or `-i` opioons don't contain the directory separator *'/'*
then `gawk` searches the directories in env variable *AWKPATH* (_search path_)
* If source code is not found after the initial search, the path is searched
again after adding the suffix `.awk` to the filename
* The default value for *AWKPATH* is `.:/usr/local/share/awk`
* Since `.` is included at the beginning, `gawk` searches first in the current
 directory and then in `/usr/local/share/awk`

=== The *AWKLIBPATH* environment variable
* Search for loadable extensions (stored as system libraries) specified with the
`-l` option rather than for source files
* If extension is not found, the path is searched with `.so` suffix
* Default path: `/usr/local/lib/gawk`

=== Other enviroment Variables
* You can get a list of other enviroment variables available
https://www.gnu.org/software/gawk/manual/html_node/Other-Environment-Variables.html#Other-Environment-Variables[here]

=== Include other files into your `gawk`program
* Using the `@include` keyword
* Specific to `gawk` programs
* The current directory is always searched first for source files

Example:

+++<u>script1:</u>+++
```
BEGIN { print "script1"}
```
+++<u>script2:</u>+++
```
@include script1
BEGIN { print "script2"}
```
+++<u>Output:</u>+++
```
$ gawk -f script2
script1
script2
```

=== Loading dynamic extensions into your program
* The ``@load` keyword can be used to read external awk extensions
* Equivalent to `-l` option
Example:

```
$ gawk '@load "ordchr"; BEGIN {print chr(65)}'
A
```

== Built in variables

* *NR*: Current count of number of input records
* *NF*: Keeps a count of number of input fields
* *FILENAME*: The name of the current input file
* *FNR*: No of records in the current filename
* *FS*: Contains the _field separator_ character
* *RS*: Stores the current _record separator_ or _row separator_
* *OFS*: Stores the _output field separator_
* *ORS*: Stores the _output record separator_

=== Examples

* `FS`, `NF`, `FILENAME`, `NR`
=====
* There are 7 fields separated by colons in /etc/passwd and 45 rows in total
```
$ awk 'BEGIN {FS=":"} END {print "Number of fields: "NF; print "Filename: "FILENAME; print "Number of records:"NR;}' /etc/passwd
Number of fields: 7
Filename: /etc/passwd
Number of records:45
```

=====

* `RS`

====

====

== Regular Expressions

== Reading Input Files

== Printing Output

== Expressions

== Patterns, Actions and Variables

== Arrays in `awk`

== Functions

== A library of `awk` Functions

== Practical `awk` programs

== Advanced features of `gawk`
